package order.stub;

import cliffberg.utilities.HttpUtilities;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;

import java.net.URL;

public class OrderFactoryLive implements OrderFactory {

	public OrderStub createOrder() throws Exception {

		return new OrderStub() {

			private String serviceURL;

			{
				this.serviceURL = System.getenv("ORDER_SERVICE_URL");
				if (serviceURL == null) throw new RuntimeException(
					"ORDER_SERVICE_URL not set in environment");
			}

			public int provision(int deptNo) throws Exception {
				/* Make the HTTP request and get the response as a string. */
				String jsonStr = HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/" + "Provision?DeptNo=" + deptNo));
				OrderResponse orderResponse = parseResponse(jsonStr);
				int orderId = orderResponse.getOrderId();
				if (orderId == 0) throw new RuntimeException(
					"Internal server error: empty order ID received from server");
				return orderId;
			}

			public void cancelOrder(int orderId) throws Exception {
				/* Make the HTTP request - we do not need the response payload. */
				HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/" + "CancelOrder?OrderId=" + orderId));
			}

			public String ping() throws Exception {
				return HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/ping"));
			}
		};
	}

	static class OrderResponse {
		public int orderId;
		public OrderResponse(int orderId) { this.orderId = orderId; }
		public void setOrderId(int orderId) { this.orderId = orderId; }
		public int getOrderId() { return this.orderId; }
	}

	private OrderResponse parseResponse(String jsonStr) throws Exception {

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		OrderResponse orderResponse;
		try {
			orderResponse = gson.fromJson(jsonStr, OrderResponse.class);
		} catch (Exception ex) {
			throw new RuntimeException("Ill-formatted JSON server response: " + jsonStr);
		}
		return orderResponse;
	}
}
