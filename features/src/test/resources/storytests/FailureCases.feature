@Failure
Feature: FailureCases

	Scenario: Order update succeeds but Fulfill update fails

		Given there are no more saddles
		When I call Provision
		Then the provision automatically cancels the griffin requisition
