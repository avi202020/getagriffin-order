package order.cdk;

import software.amazon.awscdk.core.App;

import java.util.Arrays;

public class OrderApp {
    public static void main(final String[] args) {

        App app = new App();
        new OrderStack(app, "OrderStack");
        app.synth();
    }
}
